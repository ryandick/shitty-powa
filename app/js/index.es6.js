var fs = require( 'fs' ),
	path = require( 'path' ),
	_ = require( 'lodash' ),
	moment = require( 'moment' ),
	gui = require( 'nw.gui' );
//	main store
var PROPS = {
	get suburb() {
		return this._suburb;
	},
	set suburb( val ) {
		this._suburb = val;
		ACTIONS.__store( ACTIONS.__sync( ACTIONS.__render ) );
	},
	get stage() {
		if ( this._stage ) return this._stage;
		else return 1;
	},
	set stage( val ) {
		this._stage = Number( val );
		ACTIONS.__store( ACTIONS.__sync( ACTIONS.__render ) );
	},
	details: {},
	suburbs: {},
	server: {
		get: function() {
			return 'http://localhost:8899'
		},
		enumerable: false,
		configurable: false
	},
	ttl: moment().subtract( 1, 'hour' ),
	stages: [ 1, 2, 3 ]
};
var ACTIONS = {
	selectSuburb: function( code ) {
		PROPS.suburb = code
	},
	selectStage: function( stage = 1 ) {
		PROPS.stage = Number( stage );
	},
	search: function() {
		PROPS.filter = $( '#searchField' ).val();
		ACTIONS.__store( ACTIONS.__render() );
		$( '#searchField' ).focus();
	},
	__render: function( renderWhat ) {
		function _nav() {
			let suburbs = ( PROPS.filter ) ? PROPS.suburbs.filter( suburb => {
				return ( suburb.name.toLowerCase().indexOf( PROPS.filter.toLowerCase() ) > -1 );
			} ) : PROPS.suburbs;
			suburbs = suburbs.filter( suburb => {
				return ( suburb.stage === PROPS.stage );
			} );
			$( '#nav' ).empty().mustache( 'suburbs', {
				suburbs: suburbs,
				PROPS: PROPS
			} );
		}

		function _details() {
			if ( PROPS.details[ PROPS.suburb ] && PROPS.details[ PROPS.suburb ][ PROPS.stage ] ) {
				let suburb = PROPS.details[ PROPS.suburb ][ PROPS.stage ];
				suburb.schedules = suburb.schedules.filter( schedule => {
					return ( moment().isBefore( schedule.start ) && Number( schedule.stage ) === Number( PROPS.stage ) );
				} );
				$( '#suburb-detail-contain' ).empty().mustache( 'suburb-detail', {
					suburb: suburb,
					PROPS: PROPS
				} );
				$( '#searchField' ).val( PROPS.filter );
				$( '#suburb-detail-contain' ).css( 'opacity', 1 );
			}
		}
		let details = _.throttle( _details, 100, {
			leading: true,
			trailing: false
		} );
		let nav = _.throttle( _nav, 100, {
			leading: true,
			trailing: false
		} );
		if ( renderWhat === 'nav' ) nav();
		else if ( renderWhat === 'details' ) details();
		else {
			nav();
			details();
		}
		$( '#suburb' + PROPS.suburb ).addClass( 'uk-active' );
		$( '#stage' + PROPS.stage ).addClass( 'uk-active' );
	},
	__store: function( callback ) {
		fs.writeFile( path.resolve( 'data/props.json' ), JSON.stringify( PROPS ), function( err ) {
			if ( err ) throw new Error( err );
			if ( callback ) callback();
		} );
	},
	__sync: function( callback ) {
		$( '#suburb-detail-contain' ).empty().mustache( 'loading' );
		$.get( PROPS.server + '/suburbs' ).success( function( data ) {
			PROPS.suburbs = data;
			if ( !PROPS.suburb ) PROPS.suburb = data[ 0 ].code;
			PROPS.details[ PROPS.suburb ] = PROPS.details[ PROPS.suburb ] || {};
		} );
		if ( PROPS.suburb && PROPS.stage ) $.get( PROPS.server + '/suburbs/' + PROPS.suburb + '/' + PROPS.stage ).success( function( data ) {
			PROPS.details[ PROPS.suburb ][ PROPS.stage ] = data;
			if ( callback ) callback();
		} );
		else callback();
	},
	__loadPartials: function() {
		$.Mustache.add( 'suburb-detail', fs.readFileSync( path.resolve( 'partials/suburb-detail.mst' ) ).toString( 'utf8' ) );
		$.Mustache.add( 'suburbs', fs.readFileSync( path.resolve( 'partials/suburbs.mst' ) ).toString( 'utf8' ) );
		$.Mustache.add( 'loading', fs.readFileSync( path.resolve( 'partials/loading.mst' ) ).toString( 'utf8' ) );
		$.Mustache.add( 'stages', fs.readFileSync( path.resolve( 'partials/stages.mst' ) ).toString( 'utf8' ) );
	},
	__loadProps: function() {
		PROPS = _.merge( PROPS, JSON.parse( fs.readFileSync( path.resolve( 'data/props.json' ) ) ) );
	},
	__init: function() {
		ACTIONS.__loadProps();
		ACTIONS.__loadPartials();
		ACTIONS.__render();
	}
}
jQuery( function() {
	ACTIONS.__init();
} );
$( 'body' ).on( 'keyup', '.uk-search-field', ACTIONS.search );