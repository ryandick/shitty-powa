var Agenda = require( 'agenda' );
var agenda = new Agenda( {
	db: {
		address: 'localhost:27017/powa',
		collection: 'jobs'
	}
} );
agenda.define( 'scrape schedule', {
	priority: 'high',
	concurrency: 6
}, function( job, done ) {
	console.log( 'starting scraper, jobID : "' + job.attrs + '"' );
	require( './lib/scraper' ).update( job.attrs.data.stage, done );
} );
agenda.every( '30 minutes', 'scrape schedule', {
	stage: 1
} );
agenda.every( '30 minutes', 'scrape schedule', {
	stage: 2
} );
agenda.every( '30 minutes', 'scrape schedule', {
	stage: 3
} );
agenda.now( 'scrape schedule', {
	stage: 1
} );
agenda.now( 'scrape schedule', {
	stage: 2
} );
agenda.now( 'scrape schedule', {
	stage: 3
} );

function graceful() {
	agenda.stop( function() {
		process.exit( 0 );
	} );
}
process.on( 'SIGTERM', graceful );
process.on( 'SIGINT', graceful );
agenda.start();
require( './lib/server' )();