var express = require( 'express' ),
	morgan = require( 'morgan' ),
	db = require( './data/' ),
	moment = require( 'moment' );
var app = express();
var lastMod = {
	suburbs: moment(),
	suburb: {}
};
//	middleware
app.use( morgan( ':method	:status	:url :response-time ms' ) );
//	main endpoint
app.get( '/suburbs', function( req, res, next ) {
	db.models.Suburb.find( {
		stage: 1
	} ).select( 'id name code stage' ).lean().exec( function( err, suburbs ) {
		if ( err ) next( err );
		else {
			suburbs = suburbs.map( function( suburb ) {
				suburb.value = suburb.code;
				suburb.text = suburb.name;
				return suburb;
			} );
			lastMod.suburbs = moment();
			//	set clientside caching to a day as aggregates are daily
			res.setHeader( 'cache-control', 'public, max-age=86400' );
			res.setHeader( 'Access-Control-Allow-Origin', '*' );
			res.setHeader( 'Access-Control-Allow-Methods', 'GET' );
			res.json( suburbs );
		}
	} );
} );
app.get( '/suburbs/:code', function( req, res, next ) {
	db.models.Suburb.findOne( {
		code: req.params.code
	} ).lean().exec( function( err, suburb ) {
		if ( err ) next( err );
		else {
			suburb.schedules.map( function( schedule ) {
				schedule.fmtDay = moment( schedule.start ).format( 'dddd, MMMM Do YYYY' );
				schedule.fmtDate = moment( schedule.start ).format( 'YYYY-MM-DD h:mm' );
				schedule.fmtEnd = moment( schedule.end ).format( 'h:mma' );
				schedule.fmtStart = moment( schedule.start ).format( 'h:mma' );
			} );
			suburb.fmtUpdated = moment( suburb.updated ).fromNow();
			res.setHeader( 'last-modified', suburb.updated );
			res.setHeader( 'etag', suburb._id );
			res.setHeader( 'cache-control', 'public, max-age=86400' );
			res.setHeader( 'Access-Control-Allow-Origin', '*' );
			res.setHeader( 'Access-Control-Allow-Methods', 'GET' );
			res.json( suburb );
		}
	} );
} );
app.get( '/suburbs/:code/:stage', function( req, res, next ) {
	db.models.Suburb.findOne( {
		code: req.params.code,
		stage: req.params.stage
	} ).lean().exec( function( err, suburb ) {
		if ( err ) next( err );
		else {
			suburb.schedules.map( function( schedule ) {
				schedule.fmtDay = moment( schedule.start ).format( 'dddd, MMMM Do YYYY' );
				schedule.fmtDate = moment( schedule.start ).format( 'YYYY-MM-DD h:mm' );
				schedule.fmtEnd = moment( schedule.end ).format( 'h:mma' );
				schedule.fmtStart = moment( schedule.start ).format( 'h:mma' );
			} );
			suburb.fmtUpdated = moment( suburb.updated ).fromNow();
			res.headers[ 'last-modified' ] = suburb.updated;
			res.headers[ 'etag' ] = suburb._id;
			res.setHeader( 'cache-control', 'public, max-age=86400' );
			res.setHeader( 'Access-Control-Allow-Origin', '*' );
			res.setHeader( 'Access-Control-Allow-Methods', 'GET' );
			res.json( suburb );
		}
	} );
} );
//	error handler
app.use( function( err, req, res, next ) {
	// whatever you want here, feel free to populate
	// properties on `err` to treat it differently in here.
	res.status( err.status || 500 );
	res.send( {
		error: err.message
	} );
} );
//	not found handler
app.use( function( req, res ) {
	res.status( 404 );
	res.send( {
		error: 'Lame, can\'t find that'
	} );
} );

function start() {
	app.listen( 8899, function() {
		console.log( 'server listening on: http://localhost:8899' );
	} );
}
if ( !module.parent ) start();
else module.exports = start;