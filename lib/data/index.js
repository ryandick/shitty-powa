var mongoose = require( 'mongoose' );
mongoose.connect( 'mongodb://localhost/powa' );
var SuburbsSchema = mongoose.Schema( {
	name: {
		type: String,
		required: true,
		index: true
	},
	code: {
		type: String,
		required: true
	},
	stage: {
		type: Number,
		default: 1
	},
	schedules: [ {
		SubBlock: String,
		start: Date,
		end: Date,
		stage: Number
	} ],
	created: {
		type: Date,
		default: new Date()
	},
	updated: {
		type: Date
	}
} );
SuburbsSchema.pre( 'save', function( next ) {
	this.updated = new Date();
	next();
} );
module.exports = {
	mongoose: mongoose,
	schema: {
		Suburbs: SuburbsSchema
	},
	models: {
		Suburb: mongoose.model( 'Suburb', SuburbsSchema )
	}
};