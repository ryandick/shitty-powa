var sa = require( 'superagent' ),
	moment = require( 'moment' ),
	cheerio = require( 'cheerio' ),
	async = require( 'neo-async' ),
	STAGE = process.env.STAGE = 1,
	db = require( './data/' );

function getSuburbs( callback ) {
	var suburbs = [];
	sa.get( 'https://www.citypower.co.za/customers/pages/Load_Shedding.aspx' ).end( function( resp ) {
		var $ = cheerio.load( resp.text );
		$( '.Suburb' ).children( 'select' ).children( 'option' ).each( function() {
			var name = $( this ).text();
			name = name.substring( 0, name.length - 5 );
			suburbs.push( {
				name: name,
				code: $( this ).val()
			} );
		} );
		callback( null, suburbs );
	} );
}

function getSuburb( code, stage, callback ) {
	sa.get( 'https://www.citypower.co.za/LoadSheddingSchedule.axd?Suburb=' + code + '&Stage=Stage' + stage )
		.set( 'Referer: https://www.citypower.co.za/customers/pages/Load_Shedding.aspx' )
		.end( function( resp ) {
			var result = resp.body.map( function( r ) {
				var startint = parseInt( r.StartDate.replace( '/Date(', '' ).replace( ')/', '' ) );
				var endint = parseInt( r.EndDate.replace( '/Date(', '' ).replace( ')/', '' ) );
				r.start = new Date( startint );
				r.end = new Date( endint );
				r.stage = stage || STAGE;
				return r;
			} );
			callback( null, result );
		} );
}
module.exports = {
	update: function( stage, callback ) {
		STAGE = stage || STAGE;
		getSuburbs( function( err, suburbs ) {
			console.log( suburbs.length + ' suburbs found, getting schedules for stage ' + stage || STAGE + '...' );
			var suburbCount = 0;
			async.mapLimit( suburbs, 1, function( suburb, done ) {
				if ( suburbCount === 0 ) {
					suburbCount++;
					done( err, suburb );
				} else {
					db.models.Suburb.findOne( {
						code: suburb.code,
						stage: stage || STAGE
					}, function( err, persistSuburb ) {
						suburbCount++;
						var perc = Math.round( ( ( suburbCount / suburbs.length ) * 100 ) * 100 ) / 100;
						console.log( stage + ' ' + perc + '% - ' + suburb.name );
						if ( !persistSuburb || moment( persistSuburb.updated ).isBefore( moment().subtract( 30, 'minutes' ) ) )
							getSuburb( suburb.code, stage || STAGE, function( err, schedules ) {
								suburb.schedules = ( suburb.schedules ) ? suburb.schedules : [];
								schedules.map( function( s ) {
									suburb.schedules.push( s );
								} );
								if ( !persistSuburb ) {
									suburb.stage = stage || STAGE;
									persistSuburb = new db.models.Suburb( suburb );
								}
								persistSuburb.save( done );
							} );
						else done( null, persistSuburb );
					} );
				}
			}, function( err ) {
				if ( err ) throw new Error( err );
				else if ( callback ) callback();
				else process.exit( 0 );
			} );
		} );
	}
};